public class Oven{
	public String type;
	public int wattage;
	public String brand;
	
	public void electricCostForHour(String type, int wattage){
		if (type == "gas") {
			System.out.println("A gas oven doesn't use electricity...");
		}
		else {
			System.out.println(((wattage/1000)*7)+" cents per hour");
		}
		
	}
	public void printBrandRep(String brand){
		if (brand == "bosh") {
			System.out.println("Bosh is Honestly not the best!");
		}
		else {
			System.out.println(brand+" is a pretty good brand!");
		}
	}
}