public class ApplianceStore{
	public static void main(String[] args){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		Oven[] ovens = new Oven[3];
		
		for(int i=0;i<ovens.length;i++){
			ovens[i] = new Oven();
			
			System.out.println("Please enter if Electric or Gas powered:");
			ovens[i].type = reader.next();
			System.out.println("Please enter wattage consumption:");
			ovens[i].wattage = reader.nextInt();
			System.out.println("Please enter brand:");
			ovens[i].brand = reader.next();
		}
		
		System.out.println(ovens[ovens.length-1].type);
		System.out.println(ovens[ovens.length-1].wattage);
		System.out.println(ovens[ovens.length-1].brand);
		
		ovens[0].electricCostForHour(ovens[0].type, ovens[0].wattage);
		ovens[0].printBrandRep(ovens[0].brand);
		
	}
	
	
}